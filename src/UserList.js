import UserItem from './UserItem.js';

export default class UserList extends React.Component {

    render() {

        let users = this.props.userList;

        const trUser = users.map((item, index) => (
            <UserItem
                key={index}
                user={item}
                index={index}
                editUser={this.props.editUser}
                deleteUser={this.props.deleteUser}
            />
        ));

        return <tbody>{trUser}</tbody>;
    }

}