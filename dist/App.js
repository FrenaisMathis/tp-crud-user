var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import UserList from './UserList.js';
import AddUser from './AddUser.js';
import EditUser from './EditUser.js';

var App = function (_React$Component) {
        _inherits(App, _React$Component);

        function App(props) {
                _classCallCheck(this, App);

                var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

                _this.state = {
                        userList: [],
                        isEdit: false,
                        currentUser: { id: '', nomEdit: '', prenom: '', mail: '', date: '', password: '', role: '' }
                };

                _this.users = [{ id: 1, nom: 'John', prenom: 'Doe', mail: 'Johndoe@test.com', date: '11/12/2122', password: 'test123', role: 1 }, { id: 2, nom: 'Jane', prenom: 'Doe', mail: 'Janedoe@test.com', date: '11/12/2122', password: 'test123', role: 2 }];

                _this.editUser = _this.editUser.bind(_this);
                _this.editUserSubmit = _this.editUserSubmit.bind(_this);
                _this.deleteUser = _this.deleteUser.bind(_this);
                _this.addNewUser = _this.addNewUser.bind(_this);

                return _this;
        }

        _createClass(App, [{
                key: 'componentDidMount',
                value: function componentDidMount() {

                        document.getElementById('alertConfPwd').hidden = true;

                        document.getElementById('alertConfPwdEdit').hidden = true;

                        var userList = this.users;

                        this.setState(function (prevState, props) {
                                return {
                                        userList: userList
                                };
                        });
                }
        }, {
                key: 'addNewUser',
                value: function addNewUser(nom, prenom, mail, date, pwd, pwdConf) {

                        document.getElementById('alertConfPwd').hidden = true;

                        document.getElementById('alertConfPwdEdit').hidden = true;

                        // Not reload

                        event.preventDefault();

                        // Format date

                        var newDate = new Date(date);

                        var dateFormat = newDate.getDate() + '/' + (1 + newDate.getMonth()).toString().padStart(2, '0') + '/' + newDate.getFullYear();

                        if (pwd === pwdConf) {

                                this.setState(function (prevState, props) {
                                        return {

                                                userList: [].concat(_toConsumableArray(prevState.userList), [{

                                                        id: prevState.userList.length + 1,
                                                        nom: nom,
                                                        prenom: prenom,
                                                        mail: mail,
                                                        date: dateFormat,
                                                        password: pwd,
                                                        role: 1

                                                }])

                                        };
                                });
                        } else {
                                document.getElementById('alertConfPwd').hidden = false;
                        }
                }
        }, {
                key: 'deleteUser',
                value: function deleteUser(id) {

                        document.getElementById('alertConfPwd').hidden = true;

                        document.getElementById('alertConfPwdEdit').hidden = true;

                        var alert = window.confirm("Voulez-vous supprimer cet utilisateur ?");

                        if (alert === true) {

                                // Return all users except user deleted

                                var filteredUserList = this.state.userList.filter(function (user) {
                                        return user.id !== id;
                                });

                                this.setState(function (prevState, props) {
                                        return {
                                                userList: filteredUserList
                                        };
                                });
                        }
                }
        }, {
                key: 'editUser',
                value: function editUser(user) {

                        this.setState(function (prevState, props) {
                                return {
                                        isEdit: !prevState.isEdit,
                                        currentUser: user
                                };
                        });

                        $('#formModal').modal('show');
                }
        }, {
                key: 'editUserSubmit',
                value: function editUserSubmit(nom, prenom, mail, date, pwd, pwdConf) {
                        var _this2 = this;

                        document.getElementById('alertConfPwd').hidden = true;

                        document.getElementById('alertConfPwdEdit').hidden = true;

                        event.preventDefault();

                        if (pwd === pwdConf) {

                                var newDate = new Date(date);

                                var dateFormat = newDate.getDate() + '/' + (1 + newDate.getMonth()).toString().padStart(2, '0') + '/' + newDate.getFullYear();

                                var newUserList = this.state.userList.map(function (user) {

                                        if (user.id === _this2.state.currentUser.id) {

                                                user.nom = nom;
                                                user.prenom = prenom;
                                                user.mail = mail;
                                                user.date = dateFormat;
                                                user.password = pwd;
                                        }

                                        return user;
                                });

                                this.setState(function (prevState, props) {
                                        return {
                                                userList: newUserList,
                                                isEdit: !prevState.isEdit
                                        };
                                });

                                $('#formModal').modal('hide');
                        } else {
                                document.getElementById('alertConfPwdEdit').hidden = false;
                        }
                }
        }, {
                key: 'render',
                value: function render() {

                        return React.createElement(
                                'div',
                                { className: 'container mt-5' },
                                React.createElement(
                                        'h2',
                                        null,
                                        'CRUD users'
                                ),
                                React.createElement(
                                        'table',
                                        { className: 'table' },
                                        React.createElement(
                                                'thead',
                                                null,
                                                React.createElement(
                                                        'tr',
                                                        null,
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                '#'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                'Nom'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                'Pr\xE9nom'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                'Mail'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                'Date de naissance'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'col' },
                                                                'Role'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'cole' },
                                                                'Edit'
                                                        ),
                                                        React.createElement(
                                                                'th',
                                                                { scope: 'cole' },
                                                                'Delete'
                                                        )
                                                )
                                        ),
                                        React.createElement(UserList, {
                                                deleteUser: this.deleteUser,
                                                userList: this.state.userList,
                                                editUser: this.editUser
                                        })
                                ),
                                React.createElement(
                                        'div',
                                        { className: 'alert alert-warning', role: 'alert', id: 'alertConfPwd' },
                                        'Les mots de passe sont diff\xE9rents'
                                ),
                                React.createElement(AddUser, {
                                        addNewUser: this.addNewUser
                                }),
                                React.createElement(EditUser, {
                                        editUserSubmit: this.editUserSubmit,
                                        currentUser: this.state.currentUser
                                })
                        );
                }
        }]);

        return App;
}(React.Component);

export default App;